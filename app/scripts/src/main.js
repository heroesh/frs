document.addEventListener('DOMContentLoaded', function () {

  // $('#preloader').fadeOut();
  $('body').removeClass('body-overflow');

  //svg inliner
  new SVGInliner(document.querySelectorAll(".svg-to-inline"), function () { });




  //header calc
  function calcHeader() {
    if (document.querySelector('.js-header-calc')) {
      $('.js-header-calc').css('paddingTop', $('.s_header').outerHeight());
    }
  }
  calcHeader();
  $(window).on('resize', function () {
    setTimeout(calcHeader, 100);
  })
  $(window).on('load', function () {
    setTimeout(calcHeader, 100);
  })


  //validation
  $.validator.addMethod("plus", function (value, element) {
    var Reg61 = new RegExp("^.*[^+-/(/)1234567890 ].*$");
    return !Reg61.test(value);
  });
  $.validator.addMethod("correctPassword", function (value, element) {
    if (value === $('input[name="Password"]').val()) {
      return true;
    }
    else {
      return false;
    }
  },
    "Пароли должны совпадать")

  $.validator.addMethod("notnumbers", function (value, element) {
    var Reg61 = new RegExp("^.*[^A-zА-яЁёіЇїЄєҐґ ].*$");
    return !Reg61.test(value);
  });
  //add validation rules
  var rules = {
    email: {
      required: true,
      email: true,
    },
    name: {
      required: true,
      notnumbers: true,
      minlength: 2,
    },
    place: {
      required: true,
      notnumbers: true,
      minlength: 2,
    },
    password: {
      required: true,
      minlength: 6,
    },
    passwordcorrect: {
      required: true,
      minlength: 6,
      correctPassword: true,
    },
    city: {
      required: true,
      notnumbers: true,
      minlength: 2,
    },
    surname: {
      required: true,
      notnumbers: true,
      minlength: 2,
      maxlength: 32,
    },
    phone: {
      required: true,
      plus: true,
      minlength: 10
      // digits: true,
    },
    zip: {
      required: true,
      plus: true,
      minlength: 3
      // digits: true,
    },
    theme: {
      required: true,
      minlength: 2,
    },
    question: {
      required: true,
      minlength: 2,
    },
    message: {
      required: true,
      minlength: 3,
    },
    approve: {
      required: true,
    },
    select1: {
      required: true,
    },
    select2: {
      required: true,
    },
    radio2: {
      required: true,
    },
    radio: {
      required: true,
    },
  }
  var messages = {
    email: {
      required: $('input[name="email"]').attr('data-error'),
      email: $('input[name="email"]').attr('data-error'),
    },
    name: {
      required: $('input[name="name"]').attr('data-error'),
      minlength: $('input[name="name"]').attr('data-error'),
      notnumbers: $('input[name="name"]').attr('data-error'),
    },
    surname: {
      required: $('input[name="surname"]').attr('data-error'),
      minlength: $('input[name="surname"]').attr('data-error'),
      notnumbers: $('input[name="surname"]').attr('data-error'),
    },
    place: {
      required: $('input[name="city"]').attr('data-error'),
      minlength: $('input[name="place"]').attr('data-error'),
      notnumbers: $('input[name="place"]').attr('data-error'),
    },
    theme: {
      required: $('input[name="theme"]').attr('data-error'),
      minlength: $('input[name="theme"]').attr('data-error'),
    },
    question: {
      required: $('textarea[name="question"]').attr('data-error'),
      minlength: $('textarea[name="question"]').attr('data-error'),
    },
    phone: {
      required: $('input[name="phone"]').attr('data-error'),
      digits: $('input[name="phone"]').attr('data-error'),
      plus: $('input[name="phone"]').attr('data-error'),
      minlength: $('input[name="phone"]').attr('data-error'),
    },
    password: {
      required: $('input[name="password"]').attr('data-error'),
      minlength: $('input[name="password"]').attr('data-error'),
    },
    passwordcorrect: {
      required: $('input[name="passwordcorrect"]').attr('data-error'),
      minlength: $('input[name="passwordcorrect"]').attr('data-error'),
      correctPassword: $('input[name="passwordcorrect"]').attr('data-error'),
    },
    city: {
      required: $('input[name="city"]').attr('data-error'),
      minlength: $('input[name="city"]').attr('data-error'),
      notnumbers: $('input[name="city"]').attr('data-error'),
    },
    question: {
      required: $('input[name="message"]').attr('data-error'),
      minlength: $('input[name="message"]').attr('data-error'),
    },
    zip: {
      required: $('input[name="zip"]').attr('data-error'),
      digits: $('input[name="zip"]').attr('data-error'),
      minlength: $('input[name="zip"]').attr('data-error'),
    },
    approve: {
      required: $('input[name="approve"]').attr('data-error'),
    },
    select1: {
      required: $('input[name="select1"]').attr('data-error'),
    },
    select2: {
      required: $('input[name="select2"]').attr('data-error'),
    },
    radio: {
      required: $('input[name="radio"]').attr('data-error'),
    },
    radio2: {
      required: $('input[name="radio2"]').attr('data-error'),
    },

  };

  // validation example
  if (document.querySelector('#your-id')) {
    let form = $('#your-id');
    form.validate({
      rules: rules,
      highlight: function (element, errorClass) {
        $(element).parent().addClass('input--error');
      },
      unhighlight: function (element, errorClass) {
        $(element).parent().removeClass('input--error');
      },
      messages: messages,
      submitHandler: function submitHandler(form) {
        $('#preloader').fadeIn();
        $('body').addClass('body-overflow');
        $.post('/wp-admin/admin-ajax.php?action=callback', {
          type: $('#your-id').attr('data-type'),
          name: "<p> Имя: " + $(form).find('input[name="name"]').val() + "</p>",
          phone: "<p> Телефон: " + $(form).find('input[name="phone"]').val() + "</p>",
          email: "<p> E-mail: " + $(form).find('input[name="email"]').val() + "</p>",
          text: "<p> Комментарий: " + $(form).find('textarea').val() + "</p>"
        }).done(function (data) {
          popupthanks();
          var validator = $('#your-id').validate();
          validator.resetForm();
          document.querySelector('#your-id').reset();
        }).always(function () {
          // preloader
          $('#preloader').fadeOut();
          $('body').removeClass('body-overflow');
        });
      }
    })
  }

  // validation example
  // if (document.querySelector('.js-contacts-form')) {
  //   let form = $('.js-contacts-form');
  //   form.validate({
  //     rules: rules,
  //     highlight: function (element, errorClass) {
  //       $(element).parent().addClass('input--error');
  //     },
  //     unhighlight: function (element, errorClass) {
  //       $(element).parent().removeClass('input--error');
  //     },
  //     messages: messages,
  //     submitHandler: function submitHandler(form) {
  //       $('#preloader').fadeIn();
  //       $('body').addClass('body-overflow');
  //       $.post('/wp-admin/admin-ajax.php?action=callback', {
  //         type: $('.js-contacts-form').attr('data-type'),
  //         name: $(form).find('input[name="name"]').val(),
  //         email: $(form).find('input[name="email"]').val(),
  //       }).done(function (data) {
  //         popupthanks();
  //         var validator = $('.js-contacts-form').validate();
  //         validator.resetForm();
  //         document.querySelector('.js-contacts-form').reset();
  //       }).always(function () {
  //         // preloader
  //         $('#preloader').fadeOut();
  //         $('body').removeClass('body-overflow');
  //       });
  //     }
  //   })
  // }

  //popup thank
  function popupthanks() {
    $('body').addClass('body-overflow');
    $('.s_popup').fadeOut();
    // dont forget to clear forms
    $('.s_popup_thanks').fadeIn();
    setTimeout(function () {
      $('.s_popup_thanks').fadeOut();
      $('body').removeClass('body-overflow');
    }, 3000)
  }

  // js-close popup
  if (document.querySelector('.js-popup-close')) {
    $('.js-popup-close').click(function () {
      $('.s_popup').fadeOut();
      $('body').removeClass('body-overflow');
      // dont forget to clear forms
    })
  }

  // popupmore
  $('.s_popup').mouseup(function (e) {
    var content = $('.s_popup_content');
    if (!content.is(e.target) && content.has(e.target).length === 0) {
      $('.s_popup').fadeOut();
      $('body').removeClass('body-overflow');
      //clear forms
      var validator = $('#yourformid').validate();
      validator.resetForm();
      document.querySelector('#yourformid').reset();
    }
  });

  function inWindow(s) {
    var scrollTop = $(window).scrollTop();
    var viewportBottom = scrollTop + $(window).height();

    var windowHeight = $(window).height();
    var currentEls = $(s);
    var result = [];
    currentEls.each(function () {
      var el = $(this);
      var offset = el.offset();
      if ((el.outerHeight() + offset.top) > scrollTop && offset.top < viewportBottom) {
        result.push(this);
      }
    });
    return $(result);
  }

  //burger
  if (document.querySelector('.s_header__burger')) {
    $('.s_header__burger').click(function () {
      if ($('.s_header__burger').hasClass('s_header__burger--open')) {
        $('.s_header__burger').removeClass('s_header__burger--open');
        $('.s_header__nav').removeClass('s_header__nav--open');
      $('.s_header__nav').addClass('s_header__nav--close');
        $('body').removeClass('body-overflow');
      } else {
        $('.s_header__burger').addClass('s_header__burger--open');
        $('.s_header__nav').addClass('s_header__nav--open');
        $('.s_header__nav').removeClass('s_header__nav--close');
        $('body').addClass('body-overflow');
      }
    });
  }

  if (document.querySelector('.js-mob-delay')) {
    $(".js-mob-delay").each(function (index, item) {
      $(item).css("transition-delay", .8 + .1 * index + "s")
    });
  }

  enquire.register("screen and (max-width: 1199px)", {
    match: function () {
      $('.wrapper').prepend($('.s_header__nav'));
    },
    unmatch: function () {
      $('.s_header__left').append($('.s_header__nav'));
    }
  });


  enquire.register("screen and (max-width: 767px)", {
    match: function () {
      $('.s_header__menu-wrap').append($('.s_header__link'));
      $('.s_header__menu-wrap').append($('.s_header__btn'));
    },
    unmatch: function () {
      $('.s_header__right').prepend($('.s_header__btn'));
      $('.s_header__right').prepend($('.s_header__link'));
    }
  });

  // function hoverHeader() {
  //   var array = $('.section');
  //   var max;
  //   for (var i = 0; i < array.length; i++) {
  //     if (($(array[i]).offset().top - window.innerHeight / 2) < window.scrollY) {
  //       max = $(array[i]).attr('id');
  //     }
  //   }
  //   var list = $('.js-menu');
  //   for (var i = 0; i < list.length; i++) {
  //     var str = $(list[i]).attr('href');
  //     if (str.indexOf(max) != -1) {
  //       $(list[i]).addClass('active');
  //     }
     
  //      else {
  //       $(list[i]).removeClass('active');
  //     }
  //   }

  //    if ( inWindow('.s_features').length > 0 ) {
  //     $('.js-menu').removeClass('active');
  //     $('.js-menu[href="#features"]').addClass('active');
  //   }
  // }
  // hoverHeader();
  // $(window).on('load', function () {
  //   setTimeout(hoverHeader, 100);
  // })
  // $(window).on('scroll', function () {
  //   setTimeout(hoverHeader, 100);
  // })


  // if (document.querySelector('.js-menu')) {
  //   $('.js-menu').click(function (e) {
  //     e.preventDefault();
  //     let str = $(this).attr('href').substring(1);
  //     $('html, body').stop().animate({
  //       scrollTop: $('#' + str).offset().top - $('.s_header').outerHeight()
  //     }, 400);
  //     if ($('.s_header__burger').hasClass('s_header__burger--open')) {
  //       $('.s_header__burger').removeClass('s_header__burger--open');
  //       $('.s_header__nav').removeClass('s_header__nav--open');
  //       $('body').removeClass('body-overflow');
  //     }
  //   })
  // }

  var menu_selector = ".s_header__menu-wrap"; // Переменная должна содержать название класса или идентификатора, обертки нашего меню.
var menu = $('.s_header').outerHeight();
// $(".js-menu").addClass("active");

function onScroll(){
    var scroll_top = jQuery(document).scrollTop();
    
    $(menu_selector + " a.js-menu").each(function(){
        var hash = $(this).attr("href");
        var target = $(hash);
        if (target.position().top - menu <= scroll_top && target.position().top + target.outerHeight()  > scroll_top) {
            $(menu_selector + " a.active").removeClass("active");
            $(this).addClass("active");
        } else {
            $(this).removeClass("active");
        }
    });

    if( inWindow('.s_contacts').length > 0 ) {
      $('.js-menu[href="#contacts"]').addClass('active');
    }
}
 

 
$(document).on("scroll", onScroll);

$("a.js-menu[href*='#']").click(function(e){
    e.preventDefault();

    $(document).off("scroll");
    $(menu_selector + " .active").removeClass("active");
    $(this).addClass("active");
    var hash = $(this).attr("href");
    var target = $(hash);
    if( inWindow('.s_contacts').length > 0 ) {
      $('.js-menu[href="#contacts"]').addClass('active');
    }
    if( $('.s_header__burger').hasClass('s_header__burger--open') ) {
      $('.s_header__nav').addClass('s_header__nav--close');
      $('.s_header__burger').removeClass('s_header__burger--open');
      $('.s_header__nav').removeClass('s_header__nav--open');
      $('body').removeClass('body-overflow');
    }
    $("html, body").animate({
        scrollTop: target.offset().top - $('.s_header').outerHeight()
    }, 300, function(){
        window.location.hash = hash;
        $(document).on("scroll", onScroll);
        
    });

});
  

  new WOW().init();

  let sceneOne = document.getElementById('circle-one');
  let parallaxInstanceOne = new Parallax(sceneOne);

  let sceneTwo = document.getElementById('circle-two');
  let parallaxInstanceTwo = new Parallax(sceneTwo);

  let sceneThree = document.getElementById('circle-three');
  let parallaxInstanceThree = new Parallax(sceneThree);


});
